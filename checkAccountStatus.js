const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();
const https = require('https');

const UserProfilesTableName = 'linescape-api-userProfiles-prod';
const CorporateAccountTableName = 'linescape-api-corporateAccounts-prod';

const StripeApiUrl = 'api.stripe.com';
const StripeCheckCustomerPath = '/v1/customers/';
const StripeCheckProductPath = '/v1/products/';
const StripeSecretKey = 'sk_live_2gx84k79xeTHdNgyjzIs5JzM';     // Beta: sk_test_jZARL9psHCffaKXxtHTXMmtb    Prod: sk_live_2gx84k79xeTHdNgyjzIs5JzM      

// add status code summaries for each code in comments
/*
    Succeed:
    280: General good status, nothing need to update. 
    281: Basic account, no Stripe ID related, no related corporate account, nothing can be updated. Everything OK. 
    282: Basic account, no Stripe ID related, it is covered by corporate account and updated with its corporate account status. Update succeed OK. 

    285: Have Stripe account related, account info is good, everything OK. (Non Basic account)
    286: Have Stripe account related, updated with Stripe account info, everything OK. (Non Basic account)
    
    Dynamo error: 
    580: General Dynamo error
    581: Error in get Dynamo UserProfile table
    582: Error in get Dynamo CoporateAccount table
    
    Parsing error:
    591: Failed to parse user's domain from eamil
    
    Stripe error:
    570: Failed to call Stripe API
    571: Failed to get Stripe account info form Stripe API for user
    572: Failed to get Stripe account info form Stripe API for user's administrator
    573: Multiple Stripe subscriptions existing in this user
    574: Multiple Stripe subscriptions existing in this user's administrator
    575: No Stripe subscriptions existing in this user (no longer an error, it is basic account)
    576: No Stripe subscriptions existing in this user's administrator (no longer an error, it is basic account)
    577: Failed to get related product form Stripe for user
    578: Failed to get related product form Stripe for user's administrator
*/

class NewCorporate {
    constructor(
        accountAdministrator,
        accountAdministratorCognitoId,
        accountStatus,
        accountType,
        domain, 
        endDate,
        planName,
        startDate
    ) {
        this.accountAdministrator = accountAdministrator;
        this.accountAdministratorCognitoId = accountAdministratorCognitoId;
        this.accountStatus = accountStatus;
        this.accountType = accountType;
        this.domain = domain;
        this.endDate = endDate;
        this.planName = planName;
        this.startDate = startDate;
    }
}

exports.handler = async(event, context, callback) => {
    
    let body = event;

    // Get user profile
    var userProfile = await dynamoDBGetUserProfile
        .get(body.cognitoId, UserProfilesTableName)
        .catch(err => {
            console.log("Error in Dynamo Get", err);
            const response = {
                statusCode: 581,
                body: "Error in DynamoDB get UserProfile"
            };
            return response;
        });
    
    // Get user domain
    var userDomain = "";
    try {
        userDomain = userProfile.email.split("@")[1];
    }
    catch (err) {
        const response = {
            statusCode: 591,
            body: "Error in parse user's domain from email"
        };
        return response;
    }

    // Get corporate if existing
    var corporate = await dynamoDBGetCorporateAccount
        .get(userDomain, CorporateAccountTableName)
        .catch(err => {
            console.log("Error in Dynamo Get", err);
                const response = {
                statusCode: 582,
                body: "Error in DynamoDB get CoporateAccount"
            };
        return response;
        });
        
    // Get administrator if corporate existing
    var adminUserProfile;
    if (corporate != null) {
        adminUserProfile = await dynamoDBGetUserProfile
            .get(corporate.accountAdministratorCognitoId, UserProfilesTableName)
            .catch(err => {
                console.log("Error in Dynamo Get", err);
                const response = {
                    statusCode: 581,
                    body: "Error in DynamoDB get UserProfile"
                };
                return response;
            }); 
    }
        
    // Get user's Stripe account info if Stripe ID exist
    var stripeAccountInfo;
    if (userProfile.account.stripeId != null) {
        stripeAccountInfo = await stripeCustomerHttpRequest(userProfile.account.stripeId)
        .then((data) => {
            return data;
        }).catch((e) => {
            console.log("Failed to call Stripe API");
            const response = {
                statusCode: 570,
                body: "Failed to call Stripe API"
            };
            return response;
        });
        
        if (stripeAccountInfo == null) {
            const response = {
                statusCode: 571,
                body: "Failed to get Stripe account info form Stripe API for user"
            };
            return response;
        }
    }
    
    // Get admin's Stripe account info if Stripe ID exist (it should always exist)
    var adminStripeAccountInfo;
    if (adminUserProfile != null) {
        if (adminUserProfile.account.stripeId != null) {
            adminStripeAccountInfo = await stripeCustomerHttpRequest(adminUserProfile.account.stripeId)
            .then((data) => {
                return data;
            }).catch((e) => {
                console.log("Failed to call Stripe API");
                const response = {
                    statusCode: 570,
                    body: "Failed to call Stripe API"
                };
                return response;
            });
            
            if (adminStripeAccountInfo == null) {
                const response = {
                    statusCode: 572,
                    body: "Failed to get Stripe account info form Stripe API for user's administrator"
                };
                return response;
            }
        }
    }
    
    // 
    var updatedUserProfile;
    var updatedAdminUserProfile;
    var updatedCorporate;
    var coveredByEnterpriseUpdated = false;
    
    // Check user Stripe subscription, and get user's Product Info
    var stripeProductInfo;
    if (stripeAccountInfo != null) {
        if (stripeAccountInfo.subscriptions.data.length > 1) {
            const response = {
                statusCode: 573,
                body: "Multiple Stripe subscriptions existing in this user"
            };
            return response;
        } else if (stripeAccountInfo.subscriptions.data.length < 1) {
            if (userProfile.account.type != 'Basic' || userProfile.plan.id != null) {
                // Set user to Sasic
                var editedAccountInfo = userProfile.account;
                editedAccountInfo.type = 'Basic';
                await dynamoDBUpdateUserProfileAccount.get(userProfile.cognitoId, UserProfilesTableName, editedAccountInfo);
                
                var editedPlanInfo = userProfile.plan;
                editedPlanInfo.id = null;
                editedPlanInfo.amount = null;
                editedPlanInfo.interval = null;
                editedPlanInfo.name = null;
                editedPlanInfo.trialPeriodDays = null;
                await dynamoDBUpdateUserProfilePlan.get(userProfile.cognitoId, UserProfilesTableName, editedPlanInfo);
                
                userProfile = await dynamoDBGetUserProfile.get(body.cognitoId, UserProfilesTableName).catch(err => {
                    console.log("Error in Dynamo Get", err);
                    const response = {
                        statusCode: 581,
                        body: "Error in DynamoDB get UserProfile"
                    };
                    return response;
                });
                updatedUserProfile = userProfile;
            }
        } else {
            stripeProductInfo = await stripeProductHttpRequest(stripeAccountInfo.subscriptions.data[0].plan.product).catch((err) => {
                console.log("Stripe Product api calling error - " + err);
                const response = {
                    statusCode: 570,
                    body: "Failed to call Stripe API"
                };
                return response;
            });
            if (stripeProductInfo == null) {
                const response = {
                    statusCode: 577,
                    body: "Failed to get related product form Stripe for user"
                };
                return response;
            }
        }
    }
    
    // If user is a new Enterprise admin, and there isn't a related corporate account, then create one
    if (stripeProductInfo != null && corporate == null) {
        if (stripeProductInfo.metadata.isEnterprise == 'true') {
            var startDate = new Date(0);
            var endDate = new Date(0);
            await startDate.setUTCSeconds(stripeAccountInfo.subscriptions.data[0].current_period_start);
            await endDate.setUTCSeconds(stripeAccountInfo.subscriptions.data[0].current_period_end);
            // Add the corporate
            var newCorporate = new NewCorporate(
                userProfile.email, 
                userProfile.cognitoId, 
                'active', 
                stripeProductInfo.metadata.accountType,
                userDomain,
                endDate.toString(), 
                stripeAccountInfo.subscriptions.data[0].plan.name, 
                startDate.toString()
            );

            await dynamoDBAddCorporateAccount.get(userDomain, CorporateAccountTableName, newCorporate);
        }
    }
    
    // Check admin Stripe subscription, and get admin's Product info
    var adminStripeProductInfo;
    if (adminStripeAccountInfo != null) {
        if (adminStripeAccountInfo.subscriptions.data.length > 1) {
            const response = {
                statusCode: 574,
                body: "Multiple Stripe subscriptions existing in this user's administrator"
            };
            return response;
        } else if (adminStripeAccountInfo.subscriptions.data.length < 1) {
            if (adminUserProfile.account.type != 'Basic' || adminUserProfile.plan.id != null) {
                // Set admin to Basic
                var editedAdminAccountInfo = adminUserProfile.account;
                editedAdminAccountInfo.type = 'Basic';
                await dynamoDBUpdateUserProfileAccount.get(adminUserProfile.cognitoId, UserProfilesTableName, editedAdminAccountInfo);
                
                var editedAdminPlanInfo = adminUserProfile.plan;
                editedAdminPlanInfo.id = null;
                editedAdminPlanInfo.amount = null;
                editedAdminPlanInfo.interval = null;
                editedAdminPlanInfo.name = null;
                editedAdminPlanInfo.trialPeriodDays = null;
                await dynamoDBUpdateUserProfilePlan.get(adminUserProfile.cognitoId, UserProfilesTableName, editedAdminPlanInfo);
                
                adminUserProfile = await dynamoDBGetUserProfile.get(corporate.accountAdministratorCognitoId, UserProfilesTableName).catch(err => {
                    console.log("Error in Dynamo Get", err);
                    const response = {
                        statusCode: 581,
                        body: "Error in DynamoDB get UserProfile"
                    };
                    return response;
                }); 
                updatedAdminUserProfile = adminUserProfile;
                
                // Set corporate to inactive
                var editedStatus = 'inactive';
                await dynamoDBUpdateCorporateAccountStatus.get(corporate.domain, CorporateAccountTableName, editedStatus);
                
                corporate = await dynamoDBGetCorporateAccount.get(userDomain, CorporateAccountTableName).catch(err => {
                    console.log("Error in Dynamo Get", err);
                        const response = {
                        statusCode: 582,
                        body: "Error in DynamoDB get CoporateAccount"
                    };
                    return response;
                });
                updatedCorporate = corporate;
            }
        } else {
            adminStripeProductInfo = await stripeProductHttpRequest(adminStripeAccountInfo.subscriptions.data[0].plan.product).catch((err) => {
                console.log("Stripe Product api calling error - " + err);
                const response = {
                    statusCode: 570,
                    body: "Failed to call Stripe API"
                };
                return response;
            });
            if (adminStripeProductInfo == null) {
                const response = {
                    statusCode: 578,
                    body: "Failed to get related product form Stripe for user's administrator"
                };
                return response;
            }
        }
    }
    
    // Update adminUserProfile and corporate if exist
    if (adminUserProfile != null && adminStripeAccountInfo != null && adminStripeProductInfo != null && corporate != null) {
        // Update admin user profile
        if (adminUserProfile.account.type.toLowerCase() != adminStripeProductInfo.metadata.accountType.toLowerCase() || adminUserProfile.plan.id != adminStripeAccountInfo.subscriptions.data[0].plan.id) {
            var editedAdminAccountInfo = adminUserProfile.account;
            if (adminStripeProductInfo.metadata.accountType.toLowerCase() == 'premium') {
                editedAdminAccountInfo.type = 'Premium';
                editedAdminAccountInfo.coveredByEnterprise = false;
            } else if (adminStripeProductInfo.metadata.accountType.toLowerCase() == 'professional') {
                editedAdminAccountInfo.type = 'Professional';
                editedAdminAccountInfo.coveredByEnterprise = false;
            }
            await dynamoDBUpdateUserProfileAccount.get(adminUserProfile.cognitoId, UserProfilesTableName, editedAdminAccountInfo);
            
            var editedAdminPlanInfo = adminUserProfile.plan;
            editedAdminPlanInfo.id = adminStripeAccountInfo.subscriptions.data[0].plan.id;
            editedAdminPlanInfo.amount = adminStripeAccountInfo.subscriptions.data[0].plan.amount;
            editedAdminPlanInfo.interval = adminStripeAccountInfo.subscriptions.data[0].plan.interval;
            editedAdminPlanInfo.name = adminStripeAccountInfo.subscriptions.data[0].plan.name;
            editedAdminPlanInfo.trialPeriodDays = adminStripeAccountInfo.subscriptions.data[0].plan.trial_period_days;
            await dynamoDBUpdateUserProfilePlan.get(adminUserProfile.cognitoId, UserProfilesTableName, editedAdminPlanInfo);
            
            adminUserProfile = await dynamoDBGetUserProfile.get(corporate.accountAdministratorCognitoId, UserProfilesTableName).catch(err => {
                console.log("Error in Dynamo Get", err);
                const response = {
                    statusCode: 581,
                    body: "Error in DynamoDB get UserProfile"
                };
                return response;
            }); 
            updatedAdminUserProfile = adminUserProfile;
        }
        
        // update corporate account
        if (adminStripeProductInfo.metadata.isEnterprise.toLowerCase() == 'false' && corporate.accountStatus.toLowerCase() == 'active') {
            var editedStatus = 'inactive';
            await dynamoDBUpdateCorporateAccountStatus.get(corporate.domain, CorporateAccountTableName, editedStatus);
        } else if (adminStripeProductInfo.metadata.isEnterprise.toLowerCase() == 'true' && corporate.accountStatus.toLowerCase() == 'inactive') {
            var editedStatus = 'active';
            await dynamoDBUpdateCorporateAccountStatus.get(corporate.domain, CorporateAccountTableName, editedStatus);
        } 
        if (adminStripeProductInfo.metadata.accountType != corporate.accountType) {
            var editedAccountType = adminStripeProductInfo.metadata.accountType;
            await dynamoDBUpdateCorporateAccountType.get(corporate.domain, CorporateAccountTableName, editedAccountType);
        }
        if (adminStripeAccountInfo.subscriptions.data[0].plan.name != corporate.planName) {
            var editedPlanName = adminStripeAccountInfo.subscriptions.data[0].plan.name;
            await dynamoDBUpdateCorporatePlanName.get(corporate.domain, CorporateAccountTableName, editedPlanName);
        }
        var adminStartDate = new Date(0);
        var adminEndDate = new Date(0);
        await adminStartDate.setUTCSeconds(adminStripeAccountInfo.subscriptions.data[0].current_period_start);
        await adminEndDate.setUTCSeconds(adminStripeAccountInfo.subscriptions.data[0].current_period_end);
        if (adminStartDate != corporate.startDate || corporate.startDate == null || adminEndDate != corporate.endDate || corporate.endDate == null) {
            var editedStartDate = adminStartDate.toString();
            var editedEndDate = adminEndDate.toString();
            await dynamoDBUpdateCorporateStartEndDate.get(corporate.domain, CorporateAccountTableName, editedStartDate, editedEndDate);
        }

        corporate = await dynamoDBGetCorporateAccount.get(userDomain, CorporateAccountTableName).catch(err => {
            console.log("Error in Dynamo Get", err);
                const response = {
                statusCode: 582,
                body: "Error in DynamoDB get CoporateAccount"
            };
            return response;
        });
        updatedCorporate = corporate;
    } else if (adminUserProfile != null && adminStripeAccountInfo != null && adminStripeProductInfo == null && corporate != null) {
        // If admin cancel subscription, then deactive corporate
        if (adminStripeAccountInfo.subscriptions.data.length == 0) {
            var editedStatus = 'inactive';
            await dynamoDBUpdateCorporateAccountStatus.get(corporate.domain, CorporateAccountTableName, editedStatus);
            
            var editedAccountType = 'Basic';
            await dynamoDBUpdateCorporateAccountType.get(corporate.domain, CorporateAccountTableName, editedAccountType);
            
            var editedPlanName = 'Basic';
            await dynamoDBUpdateCorporatePlanName.get(corporate.domain, CorporateAccountTableName, editedPlanName);
            
            var editedStartDate = '';
            var editedEndDate = '';
            await dynamoDBUpdateCorporateStartEndDate.get(corporate.domain, CorporateAccountTableName, editedStartDate, editedEndDate);
            
            corporate = await dynamoDBGetCorporateAccount.get(userDomain, CorporateAccountTableName).catch(err => {
                console.log("Error in Dynamo Get", err);
                    const response = {
                    statusCode: 582,
                    body: "Error in DynamoDB get CoporateAccount"
                };
                return response;
            });
            updatedCorporate = corporate;
        }
    }
    
    // Update User profile
    if (userProfile != null && stripeAccountInfo != null && stripeProductInfo != null) {
        if (userProfile.account.type.toLowerCase() != stripeProductInfo.metadata.accountType.toLowerCase() || userProfile.plan.id != stripeAccountInfo.subscriptions.data[0].plan.id) {
            var editedAccountInfo = userProfile.account;
            if (stripeProductInfo.metadata.accountType.toLowerCase() == 'premium') {
                editedAccountInfo.type = 'Premium';
                editedAccountInfo.coveredByEnterprise = false;
            } else if (stripeProductInfo.metadata.accountType.toLowerCase() == 'professional') {
                editedAccountInfo.type = 'Professional';
                editedAccountInfo.coveredByEnterprise = false;
            }
            await dynamoDBUpdateUserProfileAccount.get(userProfile.cognitoId, UserProfilesTableName, editedAccountInfo);
            
            var editedPlanInfo = userProfile.plan;
            editedPlanInfo.id = stripeAccountInfo.subscriptions.data[0].plan.id;
            editedPlanInfo.amount = stripeAccountInfo.subscriptions.data[0].plan.amount;
            editedPlanInfo.interval = stripeAccountInfo.subscriptions.data[0].plan.interval;
            editedPlanInfo.name = stripeAccountInfo.subscriptions.data[0].plan.name;
            editedPlanInfo.trialPeriodDays = stripeAccountInfo.subscriptions.data[0].plan.trial_period_days;
            await dynamoDBUpdateUserProfilePlan.get(userProfile.cognitoId, UserProfilesTableName, editedPlanInfo);
            
            userProfile = await dynamoDBGetUserProfile.get(body.cognitoId, UserProfilesTableName).catch(err => {
                console.log("Error in Dynamo Get", err);
                const response = {
                    statusCode: 581,
                    body: "Error in DynamoDB get UserProfile"
                };
                return response;
            });
            updatedUserProfile = userProfile;
        }
    }
    
    // Check if this user is covered by Enterprise
    if (corporate != null) {
        if ((corporate.accountStatus.toLowerCase() == 'active' && userProfile.account.coveredByEnterprise != true && (userProfile.account.type == 'Basic' || userProfile.account.type == null)) || 
            (corporate.accountStatus.toLowerCase() == 'inactive' && userProfile.account.coveredByEnterprise == true)) {
            var editedAccountInfo = userProfile.account;
            if (corporate.accountStatus.toLowerCase() == 'active' && userProfile.account.coveredByEnterprise != true && (userProfile.account.type == 'Basic' || userProfile.account.type == null)) {
                editedAccountInfo.coveredByEnterprise = true;
                editedAccountInfo.type = "Basic";
            } else if (corporate.accountStatus.toLowerCase() == 'inactive' && userProfile.account.coveredByEnterprise == true) {
                editedAccountInfo.coveredByEnterprise = false;
            }
            await dynamoDBUpdateUserProfileAccount.get(userProfile.cognitoId, UserProfilesTableName, editedAccountInfo);
            
            userProfile = await dynamoDBGetUserProfile.get(body.cognitoId, UserProfilesTableName).catch(err => {
                console.log("Error in Dynamo Get", err);
                const response = {
                    statusCode: 581,
                    body: "Error in DynamoDB get UserProfile"
                };
                return response;
            });
            updatedUserProfile = userProfile;
            coveredByEnterpriseUpdated = true;
        }
    } else if (userProfile.account.coveredByEnterprise != null) {
        // In case someone manually delete an corporate account from database
        if (userProfile.account.coveredByEnterprise == true) {
            var editedAccountInfo = userProfile.account;
        editedAccountInfo.coveredByEnterprise = false;
        await dynamoDBUpdateUserProfileAccount.get(userProfile.cognitoId, UserProfilesTableName, editedAccountInfo);
        
        userProfile = await dynamoDBGetUserProfile.get(body.cognitoId, UserProfilesTableName).catch(err => {
                console.log("Error in Dynamo Get", err);
                const response = {
                    statusCode: 581,
                    body: "Error in DynamoDB get UserProfile"
                };
                return response;
            });
        updatedUserProfile = userProfile;
        }
    }
    
    if ((stripeAccountInfo == null || userProfile.account.type == 'Basic') && corporate == null && updatedUserProfile != null && stripeAccountInfo == null) {
        const response = {
            statusCode: 281,
            body: updatedUserProfile
        };
        return response;
    }
    
    if ((stripeAccountInfo == null || userProfile.account.type == 'Basic') && corporate != null && updatedUserProfile != null) {
        const response = {
            statusCode: 282,
            body: updatedUserProfile
        };
        return response;
    }
    
    if (updatedUserProfile == null && updatedAdminUserProfile == null && updatedCorporate == null) {
        const response = {
            statusCode: 280,
            body: userProfile
        };
        return response;
    }
    
    if (updatedUserProfile == null && stripeAccountInfo != null) {
        const response = {
            statusCode: 285,
            body: userProfile
        };
        return response;
    }
    
    if (updatedUserProfile != null && stripeAccountInfo != null) {
        const response = {
            statusCode: 286,
            body: updatedUserProfile
        };
        return response;
    }
    
    // In case any unexpected situation
    const response = {
        statusCode: 290,
        body: userProfile
    };
    return response;
};

function stripeCustomerHttpRequest(stripeId) {
    return new Promise((resolve, reject) => {

        var host = 'api.stripe.com';
        var path = StripeCheckCustomerPath + stripeId;
        var authorization = 'Bearer ' + StripeSecretKey;

        const options = {
            host: host,
            path: path,
            //port: 443,
            method: 'GET',
            headers: {
                'Authorization': authorization
            }
        };
        const req = https.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            var body = [];
            res.on('data', function(chunk) {
                body.push(chunk);
            });
            res.on('end', function() {
                try {
                    body = JSON.parse(Buffer.concat(body).toString());
                }
                catch (e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        req.on('error', (e) => {
            reject(e.message);
        });
        // send the request
        req.end();
    });
}

function stripeProductHttpRequest(productId) {
    return new Promise((resolve, reject) => {

        var host = 'api.stripe.com';
        var path = StripeCheckProductPath + productId;
        var authorization = 'Bearer ' + StripeSecretKey;

        const options = {
            host: host,
            path: path,
            //port: 443,
            method: 'GET',
            headers: {
                'Authorization': authorization
            }
        };
        const req = https.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            var body = [];
            res.on('data', function(chunk) {
                body.push(chunk);
            });
            res.on('end', function() {
                try {
                    body = JSON.parse(Buffer.concat(body).toString());
                }
                catch (e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        req.on('error', (e) => {
            reject(e.message);
        });
        // send the request
        req.end();
    });
}

const dynamoDBGetUserProfile = {
    async get(cognitoId, TableName) {
        const params = {
            TableName,
            Key: {
                cognitoId
            }
        };

        const data = await documentClient.get(params).promise();

        console.log(data);

        return data.Item;
    }
}

const dynamoDBUpdateUserProfileAccount = {
    async get(cognitoId, TableName, editedAccountInfo) {
        const params = {
            TableName,
            Key: {
                cognitoId
            },
            UpdateExpression: "SET #account = :account",
            ExpressionAttributeValues: {
                ":account": editedAccountInfo
            },
            ExpressionAttributeNames: {
                "#account": "account"
            },
            ReturnValues: "UPDATED_NEW"
        };

        await documentClient.update(params).promise();
    }
}

const dynamoDBUpdateUserProfilePlan = {
    async get(cognitoId, TableName, editedPlanInfo) {
        const params = {
            TableName,
            Key: {
                cognitoId
            },
            UpdateExpression: "SET #plan = :plan",
            ExpressionAttributeValues: {
                ":plan": editedPlanInfo
            },
            ExpressionAttributeNames: {
                "#plan": "plan"
            },
            ReturnValues: "UPDATED_NEW"
        };

        await documentClient.update(params).promise();
    }
}

const dynamoDBGetCorporateAccount = {
    async get(domain, TableName) {
        const params = {
            TableName,
            Key: {
                domain
            }
        };

        const data = await documentClient.get(params).promise();

        console.log(data);

        return data.Item;
    }
}

const dynamoDBAddCorporateAccount = {
    async get(domain, TableName, newCorporate) {
        const params = {
            TableName, 
            Key: newCorporate.domain, 
            Item: newCorporate
        };
        
        await documentClient.put(params).promise();
    }
}

const dynamoDBUpdateCorporateAccountStatus = {
    async get(domain, TableName, editedStatus) {
        const params = {
            TableName,
            Key: {
                domain
            },
            UpdateExpression: "SET #accountStatus = :accountStatus",
            ExpressionAttributeValues: {
                ":accountStatus": editedStatus
            },
            ExpressionAttributeNames: {
                "#accountStatus": "accountStatus"
            },
            ReturnValues: "UPDATED_NEW"
        };

        await documentClient.update(params).promise();
    }
}

const dynamoDBUpdateCorporateAccountType = {
    async get(domain, TableName, editedAccountType) {
        const params = {
            TableName, 
            Key: {
                domain
            }, 
            UpdateExpression: "SET #accountType = :accountType",
            ExpressionAttributeValues: {
                ":accountType": editedAccountType
            }, 
            ExpressionAttributeNames: {
                "#accountType": "accountType"
            }, 
            ReturnValues: "UPDATED_NEW"
        };
        
        await documentClient.update(params).promise();
    }
}

const dynamoDBUpdateCorporatePlanName = {
    async get(domain, TableName, editedPlanName) {
        const params = {
            TableName, 
            Key: {
                domain
            }, 
            UpdateExpression: "SET #planName = :planName",
            ExpressionAttributeValues: {
                ":planName": editedPlanName
            },
            ExpressionAttributeNames: {
                "#planName": "planName"
            },
            ReturnValues: "UPDATED_NEW"
        };
        
        await documentClient.update(params).promise();
    }
}

const dynamoDBUpdateCorporateStartEndDate = {
    async get(domain, TableName, editedStartDate, editedEndDate) {
        const params = {
            TableName, 
            Key: {
                domain
            }, 
            UpdateExpression: "SET #startDate = :startDate, #endDate = :endDate",
            ExpressionAttributeValues: {
                ":startDate": editedStartDate, 
                ":endDate": editedEndDate
            },
            ExpressionAttributeNames: {
                "#startDate": "startDate", 
                "#endDate": "endDate"
            },
            ReturnValues: "UPDATED_NEW"
        };
        
        await documentClient.update(params).promise();
    }
}